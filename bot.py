import json
import discord
from discord.ext.commands import Bot
from discord.ext import commands
import asyncio

Client = discord.Client()
client = commands.Bot(command_prefix = "!")

@client.event
async def on_ready():
    print("Bot is online and connected to Discord")

@client.event
async def on_message(message):
    userID = message.author.id
    if message.content.lower().startswith('!update-names'):
        for member in message.server.members:
            granted = False
            print("Checking: " + str(member.nick))
            for role in member.roles:
                for allowed_role in allowed_roles['role']:
                    print("Checking: " + allowed_role['name'] + " == " + role.name)
                    if allowed_role['name'] == role.name and granted == False:
                        new_name = "[" + allowed_role['prefix'] + "] " + member.name
                        if new_name is not member.nick or member.nick is None:
                            if member.nick is None:
                                old_name = member.name
                            else:
                                old_name = member.nick
                            granted = True
                            break
            if granted == True:
                await client.change_nickname(member,new_name)
                await client.send_message(message.channel, "<@%s> %s => %s" % (userID, old_name, new_name))

with open('auth.json') as f:
    auth = json.load(f)

with open('roles.json') as f:
    allowed_roles = json.load(f)

for allowed_role in allowed_roles['role']:
    print(allowed_role['name'] + " " + allowed_role['prefix'])

client.run(auth["token"])
